//
//  Transactions.swift
//
//  Created by Atul Jain on 04/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Transactions {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let orderVia = "order_via"
    static let narration = "narration"
    static let paymentMethod = "payment_method"
    static let transId = "trans_id"
    static let rideId = "ride_id"
    static let descriptionValue = "description"
    static let receiptNumber = "receipt_number"
    static let dateTime = "date_time"
    static let driverId = "driver_id"
    static let totalAmount = "total_amount"
    static let addedBy = "added_by"
    static let transactionType = "transaction_type"
  }

  // MARK: Properties
  public var orderVia: String?
  public var narration: String?
  public var paymentMethod: String?
  public var transId: String?
  public var rideId: String?
  public var descriptionValue: String?
  public var receiptNumber: String?
  public var dateTime: String?
  public var driverId: String?
  public var totalAmount: String?
  public var addedBy: String?
  public var transactionType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    orderVia = json[SerializationKeys.orderVia].string
    narration = json[SerializationKeys.narration].string
    paymentMethod = json[SerializationKeys.paymentMethod].string
    transId = json[SerializationKeys.transId].string
    rideId = json[SerializationKeys.rideId].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    receiptNumber = json[SerializationKeys.receiptNumber].string
    dateTime = json[SerializationKeys.dateTime].string
    driverId = json[SerializationKeys.driverId].string
    totalAmount = json[SerializationKeys.totalAmount].string
    addedBy = json[SerializationKeys.addedBy].string
    transactionType = json[SerializationKeys.transactionType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = orderVia { dictionary[SerializationKeys.orderVia] = value }
    if let value = narration { dictionary[SerializationKeys.narration] = value }
    if let value = paymentMethod { dictionary[SerializationKeys.paymentMethod] = value }
    if let value = transId { dictionary[SerializationKeys.transId] = value }
    if let value = rideId { dictionary[SerializationKeys.rideId] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = receiptNumber { dictionary[SerializationKeys.receiptNumber] = value }
    if let value = dateTime { dictionary[SerializationKeys.dateTime] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = totalAmount { dictionary[SerializationKeys.totalAmount] = value }
    if let value = addedBy { dictionary[SerializationKeys.addedBy] = value }
    if let value = transactionType { dictionary[SerializationKeys.transactionType] = value }
    return dictionary
  }

}
