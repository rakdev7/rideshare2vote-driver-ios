//
//  GoONOFFLineViewController.swift
//  Apporio Taxi Driver
//
//  Created by AppOrio on 16/06/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import Firebase

class GoONOFFLineViewController: UIViewController,ParsingStates {
    
    
    @IBOutlet weak var onlinecurrentstatustextlabel: UILabel!
    
    @IBOutlet weak var youareondutytextlabel: UILabel!
    
    @IBOutlet weak var goofflinetextlabel: UILabel!
    @IBOutlet weak var currentlyyouareofftextlabel: UILabel!
    
    @IBOutlet weak var ondutytextlabel: UILabel!
    
    @IBOutlet weak var goonlinetextlabel: UILabel!
    @IBOutlet weak var offdutytextlabel: UILabel!
    @IBOutlet weak var currentstatustextlabel: UILabel!
    @IBOutlet weak var goondutyview: UIView!
    
    @IBOutlet weak var gooffdutyview: UIView!
    
    @IBOutlet weak var goofflinebtnview: UIView!
       
    @IBOutlet weak var goonlinebtnview: UIView!
    
    var viewcontrollerself : UIViewController!

    
      var ref = Database.database().reference()
      var data: OnLineOffline!
    
    
    var defaultdrivertoken = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverToken)!
    
    var onoffstatus = ""
    
    var drivername = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDrivername)!
    
    var driveremail = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverEmail)!
    
    var driverphone = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyPhoneno)!
    
    var driverdeviceid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDeviceId)!
    
    var driverimage = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverImage)!
    var driverpassword = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyPassword)!
    var driverflag = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyFlag)!
    
    var drivercartypename = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarName)!
    
    var drivercarmodelname = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarModelName)!
    
    var drivercarno = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarNo)!
    
    var drivercityid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCityId)!
    
    var drivermodelid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarModelid)!
    
    
    var loginlogoutstatus = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyLoginLogout)!
    
    
    var cartypeid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarType)!
    
    var  driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
    
    
    func setupView(){
        
        currentlyyouareofftextlabel.text = "Currently you are off duty. Please turn on your duty in order to get new rides at  your location.".localized
        currentstatustextlabel.text = "Current Status".localized
        offdutytextlabel.text = "OFF DUTY".localized
        goonlinetextlabel.text = "GO ONLINE".localized
        onlinecurrentstatustextlabel.text = "Current Status".localized
        youareondutytextlabel.text = "You are on duty. In order to turn off request from customer please turn off duty.".localized
        goofflinetextlabel.text = "GO OFFLINE".localized
        ondutytextlabel.text = "ON DUTY".localized
        
        
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        
        if GlobalVariables.checkvaluedriverpressonoffbtn == 0{
        
        goondutyview.isHidden = true
            gooffdutyview.isHidden = false
            goonlinebtnview.isHidden = false
            goofflinebtnview.isHidden = true
            
            goonlinebtnview.layer.shadowColor = UIColor.gray.cgColor
            goonlinebtnview.layer.shadowOpacity = 1
            goonlinebtnview.layer.shadowOffset = CGSize(width: 0, height: 2)
            goonlinebtnview.layer.shadowRadius = 3
            
        
        }
        if GlobalVariables.checkvaluedriverpressonoffbtn == 1{
            
            goondutyview.isHidden = false
            gooffdutyview.isHidden = true
            goonlinebtnview.isHidden = true
            goofflinebtnview.isHidden = false
            
            goofflinebtnview.layer.shadowColor = UIColor.gray.cgColor
            goofflinebtnview.layer.shadowOpacity = 1
            goofflinebtnview.layer.shadowOffset = CGSize(width: 0, height: 2)
            goofflinebtnview.layer.shadowRadius = 3

            
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ondutycancelbtn(_ sender: Any) {
        dismissViewcontroller()
        self.viewcontrollerself.viewWillAppear(true)
    }
    
    @IBAction func offdutycancelbtn(_ sender: Any) {
        dismissViewcontroller()
        self.viewcontrollerself.viewWillAppear(true)
    }
    
    @IBAction func Goofflinebtn_click(_ sender: Any) {
        
        onoffstatus = "2"
        
        
      
        
        
        APIManager.sharedInstance.delegate = self
        APIManager.sharedInstance.goOnline(driverid: driverid, onlineOffline: "2",driverToken: defaultdrivertoken)

        
        
        
    }
    
    @IBAction func Goonlinebtn_click(_ sender: Any) {
        
        onoffstatus = "1"
        
        
        
        
        APIManager.sharedInstance.delegate = self
        APIManager.sharedInstance.goOnline(driverid: driverid, onlineOffline: "1",driverToken: defaultdrivertoken)

        
    }
    
    func showAlertMessage1(_ title:String,Message:String){
        
        let alert = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { _ in
           
            
        }
        alert.addAction(action)
        self.present(alert, animated: true){}
    }
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
        if (resultCode == 88){
            if let data = data as? OnLineOffline{
                
          self.data = data
            if(self.data.result == 419){
                
                NsUserDefaultManager.SingeltonInstance.logOut()
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                self.present(next, animated: true, completion: nil)
                
                
            }else if(self.data.result == 1){
                
                if self.data.offline == 1{
                GlobalVariables.checkvaluedriverpressonoffbtn = 1
                    
                    
                     UserDefaults.standard.setValue("1", forKey:"onoffline_status")
                    
                }else if self.data.offline == 2{
                GlobalVariables.checkvaluedriverpressonoffbtn = 0
                   
                     UserDefaults.standard.setValue("2", forKey:"onoffline_status")
                    }
                
              //  self.showAlertMessage("", Message: (self.data.msg!))
                dismissViewcontroller()
                self.viewcontrollerself.viewWillAppear(true)
              
            }else{
                
                self.showAlertMessage1("", Message: (self.data.msg!))
                }
        }
            
        }
    }

   
}
