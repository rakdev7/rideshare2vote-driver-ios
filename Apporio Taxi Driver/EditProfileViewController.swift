//
//  EditProfileViewController.swift
//  Apporio Taxi Driver
//
//  Created by AppOrio on 07/06/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import Firebase


class EditProfileViewController: UIViewController, ParsingStates , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
      var data1: LogOut!
    
    var driverid: String = ""
    var OnOffData: OnLineOffline!
    
    
    var ref = Database.database().reference()
    
   
    
    var defaultdrivertoken = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverToken)!
    
    
    
    var onoffstatus = "2"

    
    var mobileNo: String = ""
    var password: String = ""
    var name: String = ""
    var driverAddressVal: String = ""
    var accountname: String = ""
    var driverZipCodeVal: String = ""
    let imageUrl = API_URLs.imagedomain
    var data: RegisterDriver!
    var defaultdriverid = ""
    var defaultdrivername = ""
    var defaultdriverphone = ""
    var defaultdriveremail = ""
    var defaultdriverPassword = ""
    var defaultdriverStreetAddress = ""
    var defaultDriverState = ""
    var defaultDriverZipCode = ""
    

    
    
    @IBOutlet var lblYourAccount: UILabel!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var lblEmailId: UILabel!
    @IBOutlet var lblPassword: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!

    
   // @IBOutlet weak var logoutbtntextlabel: UIButton!
    
    var driverdeviceid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDeviceId)!
    
    var driverimage = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverImage)!
    
    
    var driverflag = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyFlag)!
    
    var drivercartypename = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarName)!
    
    var drivercarmodelname = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarModelName)!
    
    var drivercarno = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarNo)!
    
    var drivercityid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCityId)!
    
    var drivermodelid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarModelid)!
    
    
    var loginlogoutstatus = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyLoginLogout)!
    
    
    var cartypeid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarType)!
   
    @IBOutlet weak var driverProfileImage: UIImageView!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var mobileTf: UITextField!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    
    @IBOutlet weak var driverStreetAddress: UILabel!
    @IBOutlet weak var driverStateLabel: UILabel!
     @IBOutlet weak var zipCodeLabel: UILabel!

    @IBOutlet weak var scrollview: UIScrollView!
    
    
    @IBOutlet weak var driverAddressTf: UITextField!

    @IBOutlet weak var driverZipCodeTf: UITextField!

    @IBOutlet weak var driverStateTf: UITextField!
    
    func viewSetup(){
        
      //  logoutbtntextlabel.setTitle("LOG OUT".localized, for: UIControlState.normal)
        lblYourAccount.text = "Your Account".localized
        lblEmailId.text = "EMAIL ID".localized
        lblName.text = "NAME".localized
        lblMobileNumber.text = "MOBILE NUMBER".localized
        btnDone.setTitle("DONE".localized, for: UIControlState.normal)
        lblPassword.text = "PASSWORD".localized
        
        driverStreetAddress.text = "STREET ADDRESS".localized
        driverStateLabel.text = "STATE".localized
        zipCodeLabel.text = "ZIP CODE".localized
        
        
        
    }

    


    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewSetup()
        defaultdriverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
        defaultdrivername = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDrivername)!
        defaultdriverphone = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyPhoneno)!
        defaultdriveremail = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverEmail)!
        defaultdriverPassword =  NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyPassword)!
        defaultdriverStreetAddress = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.keydriverStreetAddress)!
        defaultDriverState = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.keydriverState)!
        defaultDriverZipCode = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.keydriverZipCode)!
        
        emailTf.text = defaultdriveremail
        mobileTf.text = defaultdriverphone
        nameTf.text = defaultdrivername
        passwordTf.text = defaultdriverPassword
        driverAddressTf.text = defaultdriverStreetAddress
        driverStateTf.text = defaultDriverState
        driverZipCodeTf.text = defaultDriverZipCode

        
       
        let image = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverImage)!
        let newUrl = imageUrl + image
        let url = URL(string: newUrl)
        driverProfileImage.af_setImage(withURL:
            url! as URL,
                                       placeholderImage: UIImage(named: "dress"),
                                       filter: nil,
                                       imageTransition: .crossDissolve(1.0))
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(EditProfileViewController.imageTapped(_:)))
        driverProfileImage.isUserInteractionEnabled = true
        driverProfileImage.addGestureRecognizer(tapGestureRecognizer)
        
        driverProfileImage.layer.cornerRadius =  driverProfileImage.frame.width/2
        driverProfileImage.clipsToBounds = true
        driverProfileImage.layer.borderWidth = 1
        driverProfileImage.layer.borderColor = UIColor.black.cgColor

        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalVariables.checkphonenumber == 1{
            
            mobileTf.text! = GlobalVariables.enteruserphonenumber
                     
            GlobalVariables.checkphonenumber = 0
            
        }else{
            
        }
        
    }
    
    
    
    @IBAction func phoneeditbtn_click(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let verifyViewController = storyBoard.instantiateViewController(withIdentifier: "VerifyPhoneViewController") as! VerifyPhoneViewController
        verifyViewController.matchString = ""
        self.present(verifyViewController, animated:true, completion:nil)
        

        
    }
    
    
    override func viewWillLayoutSubviews() {
        
        
        
        super.viewWillLayoutSubviews()
        self.scrollview.frame = self.scrollview.bounds
        self.scrollview.contentSize.height = 650
        self.scrollview.contentSize.width = 0
    }
    
    
    
    @IBAction func change_password_btn(_ sender: Any) {
        
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next: ChangePasswordViewController = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.present(next, animated: true, completion: nil)

    }
    
    
    
    func imageTapped(_ img: AnyObject)
    {
        
        let alertView = UIAlertController(title: "Select Option".localized, message: "", preferredStyle: .alert)
        let Camerabutton = UIAlertAction(title: "Camera".localized, style: .default, handler: { (alert) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }

            
            
            
        })
        let Gallerybutton = UIAlertAction(title: "Gallery".localized, style: .default, handler: { (alert) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                print("Button capture")
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            }

        })
        let cancelAction = UIAlertAction(title: "Cancel".localized,
                                         style: .cancel, handler: nil)
        alertView.addAction(Camerabutton)
        alertView.addAction(Gallerybutton)
        alertView.addAction(cancelAction)
        self.present(alertView, animated: true, completion: nil)
        
        
        
       /* let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)*/
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            driverProfileImage.image = self.RBResizeImage(pickedImage, targetSize: CGSize(width: 500,height: 500))
        }
        
        dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func backbtn(_ sender: Any) {
        dismissViewcontroller()
    }

    @IBAction func Donebtn(_ sender: Any) {
        
        
        name = nameTf.text!
        mobileNo = mobileTf.text!
        password = passwordTf.text!
        
        driverAddressVal = driverAddressTf.text!
        accountname = driverStateTf.text!
        driverZipCodeVal = driverZipCodeTf.text!
       
            let parameters = [
                "driver_id": defaultdriverid,
                "driver_name": name,
                "driver_phone": mobileNo,
                "driver_token=": defaultdrivertoken,
                "language_code":GlobalVariables.languagecode
                
            ]
            
            APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.uploadRequest(parameters: parameters, driverImage: self.driverProfileImage.image!)
      //  }

    }
    
   
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
         if resultCode == 55{
        
            if let data = data as? RegisterDriver{
                
                
            self.data = data
        
        print(" Result is: \(self.data.result!)")
        if ( self.data.result == 1){
            
            
            NsUserDefaultManager.SingeltonInstance.registerDriver(insurance: (self.data.details?.insurance!)!, rc: (self.data.details?.rc!)!, licence: (self.data.details?.license!)!, did: (self.data.details?.deviceId!)!, carModelId: (self.data.details?.carModelId!)!, otherDoc: (self.data.details?.otherDocs!)!, driverId: (self.data.details?.driverId!)!, driverImg: (self.data.details?.driverImage!)!, driverEmail: (self.data.details?.driverEmail!)!, driverName: (self.data.details?.driverName!)!, flag: (self.data.details?.flag!)!, long: (self.data.details?.currentLong!)!, cityid: (self.data.details?.cityId!)!, carNo: (self.data.details?.carNumber!)!, password: (self.data.details?.driverPassword!)!, lat: (self.data.details?.currentLat!)!, phoneNo: (self.data.details?.driverPhone!)!, carType: (self.data.details?.carTypeId!)!, onOff: (self.data.details?.onlineOffline!)!, status: (self.data.details?.driverAdminStatus!)!, loginLogout: (self.data.details?.loginLogout!)!,driverToken: (self.data.details?.driverToken!)!,detailStatus : (self.data.details?.detailStatus)!,carmodelname : (self.data.details?.carModelName!)! , cartypename : (self.data.details?.carTypeName!)!,cityname : (self.data.details?.cityName!)!,Driverstatusimage: (self.data.details?.driverStatusImage)!,Driverstatusmessage: (self.data.details?.driverStatusMessage)!,DriverAddress: (self.data.details?.driverAddress) ?? "" ,DriverState: (self.data.details?.driverState) ?? "",DriverZipCode: (self.data.details?.driverZipCode) ?? "",loginstate: true)
            
            
            let alert = UIAlertController(title: "Profile Updated".localized, message:"", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: OnLineViewController = storyboard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
                
                if let window = self.view.window{
                    window.rootViewController = nextController
                }
              /* let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let next: OnLineViewController = storyboard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
                self.present(next, animated: true, completion: nil)*/
                
            }
            alert.addAction(action)
            self.present(alert, animated: true){}
        }
            
        else{
            
            let alert = UIAlertController(title: "Unable to edit!".localized, message:" Email Already Exsist or Field is Incorrect".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
                
            }
            alert.addAction(action)
            self.present(alert, animated: true){}
        }
            }
    }
    
        if resultCode == 187{
            
            if let data1 = data as? LogOut{
            
                self.data1 = data1
            
            if(self.data1.result == 419){
                
                NsUserDefaultManager.SingeltonInstance.logOut()
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                self.present(next, animated: true, completion: nil)
                
                
                
            }else if(self.data1.result == 1){
                
                
                 UserDefaults.standard.setValue("2", forKey:"onoffline_status")
                
                
               
                
                NsUserDefaultManager.SingeltonInstance.logOut()
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                self.present(next, animated: true, completion: nil)
                
            }
                
            }
        }
        
        if resultCode == 88 {
            self.OnOffData = data as! OnLineOffline
        }

        
    }

    
   
}


extension EditProfileViewController
{
    
    
    func RBResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let heightInPoints = newImage?.size.height
        let heightInPixels = heightInPoints! * (newImage?.scale)!
        print(heightInPixels)
        
        let widthInPoints = newImage?.size.width
        let widthInPixels = widthInPoints! * (newImage?.scale)!
        print(widthInPixels)
        
        
        return newImage!
    }
}

