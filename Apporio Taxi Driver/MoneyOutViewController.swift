//
//  MoneyOutViewController.swift
//  Apporio Taxi Driver
//
//  Created by Atul Jain on 04/06/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class MoneyOutViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, ParsingStates {
    
    @IBOutlet weak var newtransactiontable: UITableView!

    var viewtransactiondata : ViewTransactionModel!
    var toastLabel : UILabel!
    
    var driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
    
    var collectionsize = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-300, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        toastLabel.text =  "No Transaction!!".localized
        
        toastLabel.isHidden = true
        
        
        APIManager.sharedInstance.delegate = self
        APIManager.sharedInstance.WalletTransactionMehod(DriverId: driverid)
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return collectionsize
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = newtransactiontable.dequeueReusableCell(withIdentifier: "transactioncell", for: indexPath)
        
        
        
        
        let companycommisiontext: UILabel = (cell.contentView.viewWithTag(2) as? UILabel)!
        
        let ridernametext: UILabel = (cell.contentView.viewWithTag(3) as? UILabel)!
        
        let paymentmethodtext: UILabel = (cell.contentView.viewWithTag(4) as? UILabel)!
        
        let date: UILabel = (cell.contentView.viewWithTag(5) as? UILabel)!
        
        let pricetext: UILabel = (cell.contentView.viewWithTag(6) as? UILabel)!
        
        let image1 : UIImageView = (cell.contentView.viewWithTag(1) as? UIImageView)!
        
        
        let checkstatus = viewtransactiondata.transactions?[indexPath.row].transactionType
        
        if checkstatus == "Debit"{
        pricetext.text = "- " + GlobalVariables.currencysymbol + " " + (viewtransactiondata.transactions?[indexPath.row].totalAmount)!
        
        
        date.text = (viewtransactiondata.transactions?[indexPath.row].dateTime)!
        
        paymentmethodtext.text = "Payment Method - ".localized + (viewtransactiondata.transactions?[indexPath.row].paymentMethod)!
        
        companycommisiontext.text = (viewtransactiondata.transactions?[indexPath.row].narration)!
        
        ridernametext.text = (viewtransactiondata.transactions?[indexPath.row].narration)!
        
        ridernametext.text = "Rider Name - ".localized + (viewtransactiondata.transactions?[indexPath.row].addedBy)!
        
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView,estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
        
        let checkstatus = viewtransactiondata.transactions?[indexPath.row].transactionType
        
        if checkstatus == "Debit"{
            return 130
            
        }else{
            
            return 0
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let checkstatus = viewtransactiondata.transactions?[indexPath.row].transactionType
        
        if checkstatus == "Debit"{
            return 130
            
        }else{
            
            return 0
            
        }    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        newtransactiontable.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        print("Row: \(row)")
        
        
        
    }
    
    
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
        if resultCode == 8038{
            
            if let viewtransactiondata = data as? ViewTransactionModel{
                
                self.viewtransactiondata = viewtransactiondata
                
                if(viewtransactiondata.result == 0){
                    
                    
                    
                    toastLabel.isHidden = false
                    newtransactiontable.isHidden = true
                    
                    
                }else{
                    
                    toastLabel.isHidden = true
                    newtransactiontable.isHidden = false
                    
                    collectionsize = (viewtransactiondata.transactions?.count)!
                    
                    newtransactiontable.reloadData()
                    
                }
                
                
            }
            
        }
        
    }
    

   

}
