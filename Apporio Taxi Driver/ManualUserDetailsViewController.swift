//
//  ManualUserDetailsViewController.swift
//  Apporio Taxi Driver
//
//  Created by Atul Jain on 27/02/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase

class ManualUserDetailsViewController: UIViewController,GMSAutocompleteViewControllerDelegate,ParsingStates,MICountryPickerDelegate {
    
    var beginData: RideBegin!
    
    
    
    
    @IBOutlet weak var countrycodetext: UILabel!
    @IBOutlet weak var containerview: UIView!
    
    @IBOutlet weak var droppointtextlbl: UILabel!
    @IBOutlet weak var pickfromtextlbl: UILabel!
    @IBOutlet weak var userdetailstext: UILabel!
    
    @IBOutlet weak var pickuplocationview: UIView!
    
    @IBOutlet weak var droplocationtext: UILabel!
    @IBOutlet weak var droplocationview: UIView!
    
    @IBOutlet weak var enterusernametext: UITextField!
    
    @IBOutlet weak var enteruserphonetext: UITextField!
    
    @IBOutlet weak var pickuplocationtext: UILabel!
    
    @IBOutlet weak var startridebtntext: UIButton!
    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
    
    var cartypeid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyCarType)!
    
    var manualdroplat = ""
    
    var manualdroplong = ""
    
    var manualdroplocation = ""
    
     var selcetcountrycode = "+91"
    
     var ref = Database.database().reference()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickuplocationtext.text = "Getting Location..".localized
        userdetailstext.text = "User Details".localized
        pickfromtextlbl.text = "Pick From".localized
        droplocationtext.text = "Set your drop point".localized
        droppointtextlbl.text = "Drop Point".localized
        
startridebtntext.setTitle("Start Ride".localized, for: UIControlState.normal)
        enterusernametext.placeholder = "Enter User Name".localized
        
        enteruserphonetext.placeholder = "Enter User Phone".localized
        
        self.pickuplocationview.layer.borderWidth = 1.0
        self.pickuplocationview.layer.borderColor = UIColor.lightGray.cgColor
        self.pickuplocationview.layer.cornerRadius = 4
        
        self.containerview.layer.borderWidth = 1.0
        self.containerview.layer.borderColor = UIColor.lightGray.cgColor
        self.containerview.layer.cornerRadius = 4
        


        
        self.droplocationview.layer.borderWidth = 1.0
        self.droplocationview.layer.borderColor = UIColor.lightGray.cgColor
        self.droplocationview.layer.cornerRadius = 4
        
        pickuplocationtext.text = GlobalVariables.driverLocation
        


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
    {
        selcetcountrycode = dialCode
        countrycodetext.text = dialCode
        self.dismiss(animated: true, completion: nil)
        
        
    }

    
    
    
    @IBAction func droplocationbtnclick(_ sender: Any) {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

        
    }
    
    @IBAction func selectcountrycodebtnclick(_ sender: Any) {
        
        let picker = MICountryPicker { (name, code) -> () in
            print(code)
        }
        
        picker.delegate = self
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        picker.navigationItem.leftBarButtonItem = backButton
        // Display calling codes
        picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
        }
        
        //self.present(picker, animated: true, completion: nil)
        let navcontroller = UINavigationController(rootViewController: picker)
        
        self.present(navcontroller,animated: true,completion: nil)
        

        
    }
    
    func backButtonTapped() {
        
        dismiss(animated: true, completion: nil)
    }
    


    @IBAction func backbtnclick(_ sender: Any) {
        dismissViewcontroller()
    }
    
    
    func showAlertMessage(_ title:String,Message:String){
        
        let alert = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
            
        }
        alert.addAction(action)
        self.present(alert, animated: true){}
    }
    
    

    
    
    @IBAction func startridebtnclick(_ sender: Any) {
        
        
        if manualdroplat == ""{
        
            self.showAlertMessage("", Message: "Please enter drop location first.".localized)
        
        }else{
        
        
        APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.ManualRideMethod(UserName: enterusernametext.text!, UserPhone: selcetcountrycode + self.enteruserphonetext.text!, PickUpLat: Lat, PickUpLong: Lng, PickUpLocation: GlobalVariables.driverLocation, DropLat: self.manualdroplat, DropLong: self.manualdroplong, DropLocartion: self.manualdroplocation, CarTypprId: self.cartypeid, DriverId: self.driverid, PemFile: "1")
        
        }
        
        
        
        
    }
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
    if (resultCode == 8036){
        if let beginData = data as? RideBegin{
    
    self.beginData = beginData
    if(self.beginData.result == 419){
    
    NsUserDefaultManager.SingeltonInstance.logOut()
    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
    self.present(next, animated: true, completion: nil)
    
    
    
    }else if self.beginData.result == 1 {
    
        UserDefaults.standard.set("", forKey: "locationArray")
    GlobalVariables.trackridestatus = "6"
  
    
    GlobalVariables.rideid = (self.beginData.details?.rideId)!
      
    let alert = UIAlertController(title: "", message:" Ride Started".localized, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK".localized, style: .default) { _ in
    
    
    GlobalVariables.checkcurrentstatusvalue = 2
    
    
    
    NsUserDefaultManager.SingeltonInstance.RideAccept(DriverId: (self.beginData.details?.driverId)!, RideId: (self.beginData.details?.rideId)!, PickUpLoc: (GlobalVariables.pickupLoc), CustId: "", PickUpLat: (GlobalVariables.pickupLat), PickUpLong: (GlobalVariables.pickupLong), TrackUserName: (GlobalVariables.trackusername), TrackUserMobile: (GlobalVariables.trackusermobile), TrackDropLocation: (GlobalVariables.trackdroplocation), TrackRideStatus: (GlobalVariables.trackridestatus), DropLat: (GlobalVariables.dropLat), DropLong: (GlobalVariables.dropLong), DropLocation: (GlobalVariables.dropLocation))
        
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next: TrackRideViewController = storyboard.instantiateViewController(withIdentifier: "TrackRideViewController") as! TrackRideViewController
        
        self.present(next, animated: true, completion: nil)
    
      
    
    }
    alert.addAction(action)
    self.present(alert, animated: true){}
    
    
    }
    }
        }
    }
    
  
}


extension ManualUserDetailsViewController {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place attributions: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
        //  manualloactioncheck = "manual"
        
        droplocationtext.text = place.formattedAddress!
        
        self.manualdroplat = String(place.coordinate.latitude)
        self.manualdroplong = String(place.coordinate.longitude)
        self.manualdroplocation = place.formattedAddress!
        
        // GlobalVarible.Pickuptext = place.formattedAddress!
        // GlobalVarible.UserDropLocationText = place.formattedAddress!
     //   GlobalVariables.trackdroplocation = place.name
     //   GlobalVariables.dropLat = String(place.coordinate.latitude)
     //   GlobalVariables.dropLong = String(place.coordinate.longitude)
        
      //  self.dropchange = 1
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


