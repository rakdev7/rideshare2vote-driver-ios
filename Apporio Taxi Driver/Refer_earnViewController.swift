//
//  Refer_earnViewController.swift
//  Apporio Taxi Driver
//
//  Created by Atul Jain on 22/03/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class Refer_earnViewController: UIViewController {
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var sharecodebtntext: UIButton!

    
    @IBOutlet weak var invitecodetext: UILabel!
    @IBOutlet weak var offertypetext: UILabel!
    @IBOutlet weak var enddatetext: UILabel!
    @IBOutlet weak var startdatetext: UILabel!

    func setColor() {
        sharecodebtntext.backgroundColor = AppColors.themeColor
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setColor()
        sharecodebtntext.layer.shadowColor = UIColor.gray.cgColor
        sharecodebtntext.layer.shadowOpacity = 1
        sharecodebtntext.layer.shadowOffset = CGSize(width: 0, height: 2)
        sharecodebtntext.layer.shadowRadius = 2
        

        
        let borderLayer  = dashedBorderLayerWithColor(color: UIColor.red.cgColor)
        
        self.invitecodetext.layer.addSublayer(borderLayer)
        


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dashedBorderLayerWithColor(color:CGColor) -> CAShapeLayer {
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = invitecodetext.frame.size
        
        let shapeRect = CGRect(x: 0.0, y: 0.0, width: frameSize.width, height: frameSize.height)
        
        borderLayer.bounds=shapeRect
        borderLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.lineWidth=1.5
        borderLayer.lineJoin=kCALineJoinRound
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        
        borderLayer.path = path.cgPath
        
        return borderLayer
        
    }
    
    
    @IBAction func backbtnclick(_ sender: Any) {
        
       self.dismissViewcontroller()
    }
    
    
    @IBAction func sharecodebtnclick(_ sender: Any) {
        
        let shareText = "Hello"
        
        print(shareText)
        
        let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        self.present(activityViewController, animated: true) {
            print("option menu presented")
        }
        
        

    }
    

    

    
}
