//
//  SplashViewController.swift
//  Apporio Taxi Driver
//
//  Created by AppOrio on 06/06/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps


class SplashViewController: UIViewController,ParsingStates , CLLocationManagerDelegate {
    
     var appupdateData: AppUpdateModel!
    
    
    var data: RegisterDriver!
    var randomNumber = ""
    var city = ""
    var CITY = ""
    
       var index = 0
    let animationDuration: TimeInterval = 0.25
    let switchingInterval: TimeInterval = 3
    let locationManager = CLLocationManager()
    
   
   // @IBOutlet weak var loginasdemodrivertextdriver: UILabel!
    
    var Timer:Foundation.Timer!
    
    
    @IBOutlet weak var apporiotaxidrivertextlabel: UILabel!
    

    @IBOutlet weak var login_btn: UIButton!
    @IBOutlet weak var register_btn: UIButton!
    @IBOutlet weak var container_view: UIView!
    
    func setColor() {
        register_btn.setTitleColor(AppColors.whiteColor, for: .normal)
        register_btn.backgroundColor = AppColors.themeColor
        login_btn.backgroundColor = AppColors.whiteColor
        login_btn.setTitleColor(AppColors.themeColor, for: .normal)
        apporiotaxidrivertextlabel.textColor = AppColors.whiteColor
    }
    
    func setupView(){
        setColor()
        apporiotaxidrivertextlabel.text = "RideShare2Vote Driver".localized
   // loginasdemodrivertextdriver.text = "Login as demo driver".localized
        login_btn.setTitle("LOGIN".localized, for: UIControlState.normal)
        register_btn.setTitle("REGISTER".localized, for: UIControlState.normal)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        
        if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "en"){
            
            if GlobalVariables.languagecodeselectinmenu == 0{
                GlobalVariables.languagecode = "en"
                GlobalVariables.languageid = 1
                UserDefaults.standard.set("en", forKey: "PreferredLanguage")
                Language.language = Language(rawValue: "en")!
              
                
            }else{
                
                
            }
        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "es"){
            
            if GlobalVariables.languagecodeselectinmenu == 0{
                GlobalVariables.languagecode = "es"
                GlobalVariables.languageid = 1
                UserDefaults.standard.set("es", forKey: "PreferredLanguage")
                Language.language = Language(rawValue: "es")!
              
                
            }else{
                
                
            }
        }
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "fr"){
//
//            if GlobalVariables.languagecodeselectinmenu == 0{
//                GlobalVariables.languagecode = "fr"
//                GlobalVariables.languageid = 1
//                UserDefaults.standard.set("fr", forKey: "PreferredLanguage")
//                Language.language = Language(rawValue: "fr")!
//
//
//            }else{
//
//
//            }
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "pt"){
//
//            if GlobalVariables.languagecodeselectinmenu == 0{
//                GlobalVariables.languagecode = "pt"
//                GlobalVariables.languageid = 1
//                UserDefaults.standard.set("pt", forKey: "PreferredLanguage")
//                Language.language = Language(rawValue: "pt")!
//
//
//            }else{
//
//
//            }
//        }else if(UserDefaults.standard.object(forKey: "PreferredLanguage") as! String == "tr"){
//
//            if GlobalVariables.languagecodeselectinmenu == 0{
//                GlobalVariables.languagecode = "tr"
//                GlobalVariables.languageid = 1
//                UserDefaults.standard.set("tr", forKey: "PreferredLanguage")
//                Language.language = Language(rawValue: "tr")!
//
//
//            }else{
//
//
//            }
//        }
//        else{
//            if GlobalVariables.languagecodeselectinmenu == 0{
//
//                UserDefaults.standard.set("ar", forKey: "PreferredLanguage")
//
//                Language.language = Language(rawValue: "ar")!
//                GlobalVariables.languagecode = "ar"
//                GlobalVariables.languageid = 2
//
//            }else{
//
//
//            }
//
//        }
        
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.requestAlwaysAuthorization()
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        self.locationManager.startUpdatingLocation()
        
        
      //  if(GlobalVariables.updatechecklater == "0"){
        
        
        
         if(NsUserDefaultManager.SingeltonInstance.isloggedin()){
        
            self.container_view.isHidden = true
            APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.AppUdateMethod(ApplicationVersion: GlobalVariables.appversion)

         }else{
        
            self.container_view.isHidden = false
            APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.AppUdateMethod(ApplicationVersion: GlobalVariables.appversion)

        
        }
        
     


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    class func getColorFromHex(_ hexString:String)->UIColor{
        
        var rgbValue : UInt32 = 0
        let scanner:Scanner =  Scanner(string: hexString)
        
        scanner.scanLocation = 1
        scanner.scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0))
    }
    
    
    @IBAction func register_btn(_ sender: AnyObject) {
        
        
        
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let revealController: RegisterationViewController = storyboard.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
        self.present(revealController, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func login_btn(_ sender: AnyObject) {
        
        
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let revealController: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(revealController, animated: true, completion: nil)
       
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            reverseGeocodeCoordinate(location.coordinate)
            Lat = String(location.coordinate.latitude)
            Lng = String(location.coordinate.longitude)
            
            
        }
        
    }
    
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D)  {
        
        // 1
        
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                let lines = address.lines
                GlobalVariables.driverLocation = lines!.joined(separator: "\n")
                

                             
             //   GlobalVariables.driverLocation = address.addressLine1()! + " , " + address.addressLine2()!
                
            }
        }
    }
    
    
    func myPerformeCode(_ timer : Foundation.Timer) {
        
               Timer.invalidate()
        self.container_view.isHidden = false
        
        let transition = CATransition()
        
        //transition.type = kCATransitionFade
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        container_view.layer.add(transition, forKey: kCATransition)
        
        
        
        // here code to perform
    }
    
    func myPerformeCode1(_ timer : Foundation.Timer) {
        
        
        GlobalVariables.locationdidactive = 1
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextController: OnLineViewController = storyboard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
        
        if let window = self.view.window{
            window.rootViewController = nextController
        }

        
        /*let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let revealViewController:OnLineViewController = storyBoard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
              self.present(revealViewController, animated:true, completion:nil)*/
        
        
    }
    
    func myPerformeCode2(_ timer : Foundation.Timer) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next: UploadDocumentViewController = storyboard.instantiateViewController(withIdentifier: "UploadDocumentViewController") as! UploadDocumentViewController
        self.present(next, animated: true, completion: nil)
        
        
        
        
    }
    
    @IBAction func demodriverbtn_click(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next: CreateDemoDriverViewController = storyboard.instantiateViewController(withIdentifier: "CreateDemoDriverViewController") as! CreateDemoDriverViewController
        next.modalPresentationStyle = .overCurrentContext
        self.present(next, animated: true, completion: nil)
        
    }
    
    
    
    
    // ************************** Success state ********************************
    
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
        if resultCode == 8032 {
            
            if let appupdateData = data as? AppUpdateModel{
            
            self.appupdateData = appupdateData
           
            
            if(self.appupdateData.appcheck == 1){
                
                GlobalVariables.playerid = self.appupdateData.playerid!
                
                 GlobalVariables.updatechecklater = self.appupdateData.appcheck!
                
                if(self.appupdateData.details?.iosDriverMaintenanceMode == "1"){
                    
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}
                    
                }else{
                    

                // print(appVersion)
                let message: String = "New Version Available. Please update the app first.".localized
                let alertController = UIAlertController(
                    title: "UPDATE AVAILABLE ".localized, // This gets overridden below.
                    message: message,
                    preferredStyle: .alert
                )
                let okAction = UIAlertAction(title: "Update App".localized, style: .cancel) { _ -> Void in
                    
                    GlobalVariables.rateApp(appId: "id1439888632") { success in
                        
                    }
                    
                    
                }
                alertController.addAction(okAction)
                
                 self.present(alertController, animated: true, completion: nil)
                
                }
                
            }
                
            else if(self.appupdateData.appcheck == 2){
                
                
                if(self.appupdateData.details?.iosDriverMaintenanceMode == "1"){
                    
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}
                    
                }else{

                
                
                let refreshAlert = UIAlertController(title: "UPDATE AVAILABLE ".localized, message: "New Version Available. Please update the app.".localized, preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Update App".localized, style: .default, handler: { (action: UIAlertAction!) in
                    
                    GlobalVariables.rateApp(appId: "id1439888632") { success in
                        
                    }
                    

                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Later".localized, style: .default, handler: { (action: UIAlertAction!) in
                    
                    if(NsUserDefaultManager.SingeltonInstance.isloggedin()){
                        
                        
                        let defaultdetailstatus = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDetailStatus)!
                        
                        
                        if defaultdetailstatus == "1"{
                            
                            APIManager.sharedInstance.delegate = self
                            APIManager.sharedInstance.sendDeviceId()
                            
                            
                            
                            self.container_view.isHidden = false
                            self.Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                            
                        }else if defaultdetailstatus == "3"{
                            
                            APIManager.sharedInstance.delegate = self
                            APIManager.sharedInstance.sendDeviceId()
                            
                            
                            
                            self.container_view.isHidden = false
                            self.Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                            
                        }
                            
                            
                        else{
                            
                            APIManager.sharedInstance.delegate = self
                            APIManager.sharedInstance.sendDeviceId()
                            
                            
                            
                            self.container_view.isHidden = true
                            self.Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode1(_:)), userInfo: nil, repeats: false)
                            
                        }
                        
                    }
                    else{
                        
                        self.container_view.isHidden = false
                        
                        self.Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                        
                    }
                    

                    
                    
                }))
                
                present(refreshAlert, animated: true, completion: nil)
                
                }
            }
                
            else{
                
                
                if(self.appupdateData.details?.iosDriverMaintenanceMode == "1"){
                    
                    
                    let alert = UIAlertController(title: "", message: "App is under maintance.".localized, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Exit".localized, style: .default) { _ in
                        exit(0)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true){}
                    
                }else{

                    
                if(NsUserDefaultManager.SingeltonInstance.isloggedin()){
                    
                    
                    let defaultdetailstatus = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDetailStatus)!
                    
                    
                    if defaultdetailstatus == "1"{
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.sendDeviceId()
                        
                        
                        
                        self.container_view.isHidden = false
                        Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                        
                    }else if defaultdetailstatus == "3"{
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.sendDeviceId()
                        
                        
                        
                        self.container_view.isHidden = false
                        Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                        
                    }
                        
                        
                    else{
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.sendDeviceId()
                        
                        
                        
                        self.container_view.isHidden = true
                        Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode1(_:)), userInfo: nil, repeats: false)
                        
                    }
                    
                }
                else{
                    
                    self.container_view.isHidden = false
                    
                    Timer  = Foundation.Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(SplashViewController.myPerformeCode(_:)), userInfo: nil, repeats: false)
                    
                }
                    
                }

                
            }
            
        }

        }
        
    }


   

}
