//
//  ViewTransactionModel.swift
//
//  Created by Atul Jain on 04/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ViewTransactionModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let lowAlertBalanceStatus = "low_alert_balance_status"
    static let result = "result"
    static let walletBalance = "wallet_balance"
    static let transactions = "transactions"
  }

  // MARK: Properties
  public var message: String?
  public var lowAlertBalanceStatus: String?
  public var result: Int?
  public var walletBalance: String?
  public var transactions: [Transactions]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    message = json[SerializationKeys.message].string
    lowAlertBalanceStatus = json[SerializationKeys.lowAlertBalanceStatus].string
    result = json[SerializationKeys.result].int
    walletBalance = json[SerializationKeys.walletBalance].string
    if let items = json[SerializationKeys.transactions].array { transactions = items.map { Transactions(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = lowAlertBalanceStatus { dictionary[SerializationKeys.lowAlertBalanceStatus] = value }
    if let value = result { dictionary[SerializationKeys.result] = value }
    if let value = walletBalance { dictionary[SerializationKeys.walletBalance] = value }
    if let value = transactions { dictionary[SerializationKeys.transactions] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
