//
//  AppColor.swift
//  TaxiAppDriver
//
//  Created by Rakesh kumar on 23/08/16.
//  Copyright © 2016 Apporio. All rights reserved.
//

var APP_COLOR_PRIMARY = "3C9EA4"
var APP_COLOR_PRIMARY2 = "3C9EAB"

struct AppColors {
    
    static let themeColor = UIColor(r: 0, g: 66, b: 130, a: 1)
    static let whiteColor = UIColor(r: 255, g: 255, b: 255, a: 1)
    static let blackColor = UIColor(r: 0, g: 0, b: 0, a: 1)
    
}

