//
//  ACViewController.swift
//  CabShe
//
//  Created by Piyush Kumar on 1/9/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit


class ACViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var Webview: UIWebView!
    
 //  var webView: WKWebView?
    
    var ProductLink = ""
    
    var urlString = ""
    
 //    let userContentController = WKUserContentController()
    
    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!

    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProductLink = GlobalVariables.DriverBaseUrl + "driver_card.php?driver_id=\(driverid)&flag=\("1")"
        
        print(ProductLink)
        
        let url = NSURL(string: ProductLink)
        let request = URLRequest(url: url! as URL)
        Webview.delegate = self
        
        ActivityIndicator.hidesWhenStopped = true
        ActivityIndicator.startAnimating()
        Webview.loadRequest(request)
        
      
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func Back_Btn(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
    }
    
   
    
    
   
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        ActivityIndicator.stopAnimating()
        
        
        if let yourTargetUrl = Webview.request?.url
        {
            urlString = yourTargetUrl.absoluteString
            
            if urlString.contains("select_card.php") {
                let cardurl = (urlString.components(separatedBy: "="))
                
                let cardid = (cardurl[1])
                print (cardid)
                GlobalVariables.CardId = cardid
               
                GlobalVariables.Walletcheck = 3
                self.dismiss(animated: true, completion: nil)   
            }
            
            print(yourTargetUrl)
            
        }

    }
    
    
   

}
