//
//  AllTransactionViewController.swift
//  Apporio Taxi Driver
//
//  Created by Atul Jain on 04/06/18.
//  Copyright © 2018 apporio. All rights reserved.
//

import UIKit

class AllTransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, ParsingStates {
    
     @IBOutlet weak var newtransactiontable: UITableView!
    
     var viewtransactiondata : ViewTransactionModel!
    var toastLabel : UILabel!
    
    var driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
    
    var collectionsize = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-300, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        toastLabel.text =  "No Transaction!!".localized
        
        toastLabel.isHidden = true
        
        
        APIManager.sharedInstance.delegate = self
        APIManager.sharedInstance.WalletTransactionMehod(DriverId: driverid)
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return collectionsize
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = newtransactiontable.dequeueReusableCell(withIdentifier: "transactioncell", for: indexPath)
        
        
       
        
        let companycommisiontext: UILabel = (cell.contentView.viewWithTag(2) as? UILabel)!
        
        let ridernametext: UILabel = (cell.contentView.viewWithTag(3) as? UILabel)!
        
         let paymentmethodtext: UILabel = (cell.contentView.viewWithTag(4) as? UILabel)!
        
        let date: UILabel = (cell.contentView.viewWithTag(5) as? UILabel)!
        
         let pricetext: UILabel = (cell.contentView.viewWithTag(6) as? UILabel)!
        
        let image1 : UIImageView = (cell.contentView.viewWithTag(1) as? UIImageView)!
        
       
        
      
      
        
        date.text = (viewtransactiondata.transactions?[indexPath.row].dateTime)!
        
        paymentmethodtext.text = "Payment Method - ".localized + (viewtransactiondata.transactions?[indexPath.row].paymentMethod)!
        
        companycommisiontext.text = (viewtransactiondata.transactions?[indexPath.row].narration)!
        
        ridernametext.text = (viewtransactiondata.transactions?[indexPath.row].narration)!
//
//        price.text = Glo + " " + (viewtransactiondata.details?[indexPath.row].amount)!
//
//        date.text = viewtransactiondata.details?[indexPath.row].date
//
        if viewtransactiondata.transactions?[indexPath.row].transactionType == "Debit"{

            ridernametext.text = "Rider Name - ".localized + (viewtransactiondata.transactions?[indexPath.row].addedBy)!
              pricetext.text = "- " + GlobalVariables.currencysymbol + " " + (viewtransactiondata.transactions?[indexPath.row].totalAmount)!
             pricetext.textColor = AllTransactionViewController.getColorFromHex(hexString: "#e67e22")
            image1.image = UIImage(named: "Debit")
        }else if viewtransactiondata.transactions?[indexPath.row].transactionType == "Credit"{
           ridernametext.text = "Credit by - ".localized + (viewtransactiondata.transactions?[indexPath.row].addedBy)!
              pricetext.text = "+ " + GlobalVariables.currencysymbol + " " + (viewtransactiondata.transactions?[indexPath.row].totalAmount)!
            pricetext.textColor = AllTransactionViewController.getColorFromHex(hexString: "#2ecc71")
            image1.image = UIImage(named: "Credit")

        }else{

        }
        
        
        
        
        return cell
        
    }
    
    class func getColorFromHex(hexString:String)->UIColor{
        
        var rgbValue : UInt32 = 0
        let scanner:Scanner =  Scanner(string: hexString)
        
        scanner.scanLocation = 1
        scanner.scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        newtransactiontable.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        print("Row: \(row)")
        
        
        
    }
    
    
    
    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
        if resultCode == 8038{
            
            if let viewtransactiondata = data as? ViewTransactionModel{
                
                self.viewtransactiondata = viewtransactiondata
                
                if(viewtransactiondata.result == 0){
                    
                    
                    
                    toastLabel.isHidden = false
                    newtransactiontable.isHidden = true
                    
                    
                }else{
                    
                    toastLabel.isHidden = true
                    newtransactiontable.isHidden = false
                    
                    collectionsize = (viewtransactiondata.transactions?.count)!
                    
                    newtransactiontable.reloadData()
                    
                }
    
  
            }
            
        }
        
    }

   

}
