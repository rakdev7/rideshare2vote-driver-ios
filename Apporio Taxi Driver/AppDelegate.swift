//
//  AppDelegate.swift
//  Apporio Taxi Driver
//
//  Created by AppOrio on 05/06/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import AVFoundation
import Firebase
import UserNotifications
import GooglePlaces
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate,ParsingStates,OSSubscriptionObserver, OSPermissionObserver {

    var window: UIWindow?
    
    var part1: String = ""
    var part2: String = ""
    var part3: String = ""
    
    var rentalridesyncdata: RentalRideSyncModel!
    
    var appupdateData: AppUpdateModel!
    
    var driversyncdata: DriverSyncModel!
    
    var storyboard: UIStoryboard = UIStoryboard()
    
   var notifToDelete = ""
    
    var locationManager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            self.getCurrentAddress()
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            self.getCurrentAddress()
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            self.getCurrentAddress()
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
            
        }
    }
    
    func getCurrentAddress()
    {
        
        
        let locManager = CLLocationManager()
        // var currentLocation = CLLocation()
        
        // locManager.requestWhenInUseAuthorization()
        
        
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            
            if let   currentLocation = locManager.location
            {
                
                
                reverseGeocodeCoordinate(currentLocation.coordinate)
                
            }
            
        }
    }
    
    
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D)  {
        
        // 1
        
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                
                //  print(address.lines)
                // let lines = address.lines! as! [String]
                
                
                if let city = address.locality{
                    GlobalVariables.city  = String(city)
                    
                }
                else{
                    GlobalVariables.city = ""
                    
                }
                
                
            }
        }
    }



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            GlobalVariables.appversion = version
            print(version)
        }
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        
        
//        let center = UNUserNotificationCenter.current()
//        
//        center.getPendingNotificationRequests { (notifications) in
//            print(notifications)
//            for item in notifications {
//                if(item.identifier.contains(self.notifToDelete))
//                {
//                    center.removePendingNotificationRequests(withIdentifiers: [item.identifier])
//                    
//                }
//            }
//        }
        
        GMSServices.provideAPIKey("AIzaSyAZxmoT7SHZ3dDVTcaKELKVj-IunT5x_2Y")
        IQKeyboardManager.sharedManager().enable = true
         GMSPlacesClient.provideAPIKey("AIzaSyAZxmoT7SHZ3dDVTcaKELKVj-IunT5x_2Y")
        
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        
        UserDefaults.standard.setValue(device_id, forKey:"unique_number")

    
        
        
        
        let langId = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as! String
        
        
        
        if ((UserDefaults.standard.string(forKey: "PreferredLanguage")) != nil){
            GlobalVariables.languagecodeselectinmenu = 1
            GlobalVariables.languagecode = UserDefaults.standard.string(forKey: "PreferredLanguage")!
            // Language.language = Language(rawValue: langId!)!
            
        } else{
            
            GlobalVariables.languagecode = langId
            UserDefaults.standard.set(langId, forKey: "PreferredLanguage")
            // langId = NSLocale.current.languageCode
            // Language.language = Language(rawValue: langId!)!
            
        }

        
        FirebaseApp.configure()
        
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            print("Received Notification: \(notification!.payload.additionalData["ride_status"])")
            
          
            
           
            
            if notification!.payload.additionalData != nil {
                
                self.part1 = (notification?.payload.body)!
                self.part3 = (notification!.payload.additionalData["ride_status"] as? String)!
                self.part2 = (notification!.payload.additionalData["ride_id"] as? String)!
                
                if(self.part3 == "104"){
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "acceptscreen"), object: nil, userInfo: nil)
                    
                    
                }
                if(self.part3 == "108"){
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "rentalacceptscreen"), object: nil, userInfo: nil)
                    
                    
                }
                
                if(self.part3 == "1"){
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    GlobalVariables.rideid = self.part2
                    
                    UserDefaults.standard.setValue("1", forKey:"firebaseride_status")
                    
                    //  GlobalVariables.firebasedriverride = 1
                    
                    if (GlobalVariables.firebasedriverride == 1){
                        
                        
                    }else{
                        
                        
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.DriverSync(self.part2, DriverId: driverid)
                    }
                    
                    
                }
                
                if(self.part3 == "101"){
                    
                    if  GlobalVariables.logincheck == 1{
                        
                    }else{
                    
                    NsUserDefaultManager.SingeltonInstance.logOut()
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                    var presentedVC = self.window?.rootViewController
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(next, animated: true, completion: nil)
                    }
                    
                    
                }
                
                if(self.part3 == "54"){
                    
                    GlobalVariables.rideid = self.part2
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextController: RideLaterAcceptViewController = storyboard.instantiateViewController(withIdentifier: "RideLaterAcceptViewController") as! RideLaterAcceptViewController
                    var presentedVC = self.window?.rootViewController
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(nextController, animated: true, completion: nil)
                    
                    
                    
                }
                
                if(self.part3 == "2"){
                    
                    
                    self.showalert("Ride has been cancelled by user".localized)
                    
                    
                }
                if(self.part3 == "51"){
                    
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextController: NotificationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                    
                    
                    var presentedVC = self.window?.rootViewController
                    
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(nextController, animated: true, completion: nil)
                    
                    
                    
                    
                }
                
                
                if(self.part3 == "52"){
                    
                    GlobalVariables.dialogopen = "1"
                    self.showalert10(message: self.part1)
                }
                
                
                
                
                
                if(self.part3 == "20"){
                    GlobalVariables.rideid = self.part2
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    APIManager.sharedInstance.delegate = self
                    APIManager.sharedInstance.DriverSyncHomeScreeen(DriverId: driverid)
                    
                    
                    
                    
                }
                
                
                if(self.part3 == "8"){
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    GlobalVariables.rideid = self.part2
                    
                    APIManager.sharedInstance.delegate = self
                    APIManager.sharedInstance.DriverSync(self.part2, DriverId: driverid)
                    
                    
                }
                
                
                if(self.part3 == "17"){
                    
                    
                    self.showalert("Ride has been cancelled by Admin".localized)
                    
                    
                }
                
                
                if(self.part3 == "10"){
                    
                    
                    
                                     UserDefaults.standard.setValue("10", forKey:"firebaseride_status")
                    
                                    if (GlobalVariables.firebasedriverride == 1){
                    
                    
                                    }else{
                    
                                   GlobalVariables.rideid = self.part2
                    
                                    APIManager.sharedInstance.delegate = self
                                    APIManager.sharedInstance.RentalDriverSync(RentalBookindId: self.part2)
                                    }
                    
                                }
                
                                if(self.part3 == "15"){
                    
                    
                                    GlobalVariables.rideid = self.part2
                    
                                      self.showalert("Ride has been cancelled by user".localized)
                    
                                 //   APIManager.sharedInstance.delegate = self
                                    // APIManager.sharedInstance.RentalDriverSync(RentalBookindId: part2)
                    
                    
                                }
                
                
                
                
                                if(self.part3 == "19"){
                    
                    
                                 //   GlobalVariables.rideid = part2
                    
                                    self.showalert("Ride has been cancelled by Admin".localized)
                    
                    
                                  //  APIManager.sharedInstance.delegate = self
                                  //  APIManager.sharedInstance.RentalDriverSync(RentalBookindId: part2)
                    
                    
                                }
                
                
                
                
                
            }else{
                
                
                
            }
            
            
            
        }
        
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            
            let payload: OSNotificationPayload = result!.notification.payload
            
            if payload.additionalData != nil {
                
                 self.part1 = (payload.body)!
                self.part3 = (payload.additionalData["ride_status"] as? String)!
                self.part2 = (payload.additionalData["ride_id"] as? String)!
                
                
                if(self.part3 == "104"){
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "acceptscreen"), object: nil, userInfo: nil)
                    
                    
                }
                if(self.part3 == "108"){
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "rentalacceptscreen"), object: nil, userInfo: nil)
                    
                    
                }
                
                if(self.part3 == "1"){
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    GlobalVariables.rideid = self.part2
                    
                    UserDefaults.standard.setValue("1", forKey:"firebaseride_status")
                    
                    //  GlobalVariables.firebasedriverride = 1
                    
                    if (GlobalVariables.firebasedriverride == 1){
                        
                        
                    }else{
                        
                        
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.DriverSync(self.part2, DriverId: driverid)
                    }
                    
                    
                }
                
                if(self.part3 == "101"){
                    
                    if  GlobalVariables.logincheck == 1{
                        
                    }else{
                    
                    NsUserDefaultManager.SingeltonInstance.logOut()
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                    var presentedVC = self.window?.rootViewController
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(next, animated: true, completion: nil)
                    
                    }
                    
                }
                
                if(self.part3 == "54"){
                    
                    GlobalVariables.rideid = self.part2
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextController: RideLaterAcceptViewController = storyboard.instantiateViewController(withIdentifier: "RideLaterAcceptViewController") as! RideLaterAcceptViewController
                    var presentedVC = self.window?.rootViewController
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(nextController, animated: true, completion: nil)
                    
                    
                    
                }
                
                if(self.part3 == "2"){
                    
                    
                    self.showalert("Ride has been cancelled by user".localized)
                    
                    
                }
                if(self.part3 == "51"){
                    
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextController: NotificationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                    
                    
                    var presentedVC = self.window?.rootViewController
                    
                    while (presentedVC!.presentedViewController != nil)  {
                        presentedVC = presentedVC!.presentedViewController
                    }
                    presentedVC!.present(nextController, animated: true, completion: nil)
                    
                    
                    
                    
                }
                
                
                if(self.part3 == "52"){
                    
                    
                    
                    if  GlobalVariables.dialogopen == "0"{
                        
                        
                        self.showalert10(message: self.part1)
                    }else{
                        
                        
                    }
                }
                
                
                
                
                
                if(self.part3 == "20"){
                    GlobalVariables.rideid = self.part2
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    APIManager.sharedInstance.delegate = self
                    APIManager.sharedInstance.DriverSyncHomeScreeen(DriverId: driverid)
                    
                    
                    
                    
                }
                
                
                if(self.part3 == "8"){
                    
                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
                    
                    GlobalVariables.rideid = self.part2
                    
                    APIManager.sharedInstance.delegate = self
                    APIManager.sharedInstance.DriverSync(self.part2, DriverId: driverid)
                    
                    
                }
                
                
                if(self.part3 == "17"){
                    
                    
                    self.showalert("Ride has been cancelled by Admin".localized)
                    
                    
                }
                
                
                
                if(self.part3 == "10"){
                    
                    
                    
                    UserDefaults.standard.setValue("10", forKey:"firebaseride_status")
                    
                    if (GlobalVariables.firebasedriverride == 1){
                        
                        
                    }else{
                        
                        GlobalVariables.rideid = self.part2
                        
                        APIManager.sharedInstance.delegate = self
                        APIManager.sharedInstance.RentalDriverSync(RentalBookindId: self.part2)
                    }
                    
                }
                
                if(self.part3 == "15"){
                    
                    
                    GlobalVariables.rideid = self.part2
                    
                    self.showalert("Ride has been cancelled by user".localized)
                    
                  
                }
                
                
                
                
                if(self.part3 == "19"){
                    
                    
                    //   GlobalVariables.rideid = part2
                    
                    self.showalert("Ride has been cancelled by Admin".localized)
                    
                 
                    
                }
                
                
                
            }else{
                
                
                
            }
            
            
        }
        
        
        let onesignalInitSettings: [AnyHashable: Any] = [kOSSettingsKeyAutoPrompt: false,kOSSettingsKeyInFocusDisplayOption: OSNotificationDisplayType.none.rawValue]
        
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "f392711a-fe92-4a82-962e-80b8df66f367",
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        
        
        
        
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        OneSignal.promptLocation()
        
        OneSignal.add(self as OSSubscriptionObserver)
      
         OneSignal.add(self as OSPermissionObserver)
        

        return true
    }
    
    
   
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            
            
            UserDefaults.standard.setValue(playerId, forKey:"device_key")
            
            if GlobalVariables.afterallownotificationsetting == 1{
              GlobalVariables.afterallownotificationsetting = 0
            APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.sendDeviceId()
            }else{
                
                
            }
        }
    }
    
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
               
            } else if stateChanges.to.status == OSNotificationPermission.denied {
              
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
                
                
                
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(stateChanges)")
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        
          GlobalVariables.timerForGetDriverLocation5.invalidate()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        
        
        GlobalVariables.timerForGetDriverLocation5.fire()
        
        
        if(GlobalVariables.updatechecklater == 1){
            
            
                      
            
            // print(appVersion)
            let message: String = "New Version Available. Please update the app first.".localized
            let alertController = UIAlertController(
                title: "UPDATE AVAILABLE ".localized, // This gets overridden below.
                message: message,
                preferredStyle: .alert
            )
            let okAction = UIAlertAction(title: "Update App".localized, style: .cancel) { _ -> Void in
                
                GlobalVariables.rateApp(appId: "id1439888632") { success in
                    
                }
                
                
            }
            alertController.addAction(okAction)
            
            
            
            var presentedVC = self.window?.rootViewController
            
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            
            presentedVC!.present(alertController, animated: true) {
                
            }
            
            
        }
            
        else if(GlobalVariables.updatechecklater == 2){
            
            
            
            let refreshAlert = UIAlertController(title: "UPDATE AVAILABLE ".localized, message: "New Version Available. Please update the app.".localized, preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Update App".localized, style: .default, handler: { (action: UIAlertAction!) in
                
                GlobalVariables.rateApp(appId: "id1439888632") { success in
                    
                }
                
                
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Later".localized, style: .default, handler: { (action: UIAlertAction!) in
                
             
                
            }))
            
            var presentedVC = self.window?.rootViewController
            
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            
            presentedVC!.present(refreshAlert, animated: true) {
                
            }
            
           
            // present(refreshAlert, animated: true, completion: nil)
            
            
        }
            
            
        else{
            
          
        
        }

        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
      
        
        if GlobalVariables.playerid == "1"
        {
            
      
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Notifications are allowed
            }
            else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationscreen"), object: nil, userInfo: nil)
                
            }
        }
        
        }else{
            
            
        }
      
        if  GlobalVariables.locationdidactive == 0{
            
            
        }else{
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
               self.showalert5(message: "To continue, let your device turn on location, which uses Google's location services.Please turn on your location from settings.".localized)
                
               // This Application required location permissions to run this app. Please turn on your location from settings.
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
            
        }
        
        
        
      //  APIManager.sharedInstance.delegate = self
     //   APIManager.sharedInstance.AppUdateMethod(ApplicationVersion: GlobalVariables.appversion)
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
//    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
//        print("Notifications status: \(notificationSettings)")
//        
//        if notificationSettings.types == UIUserNotificationType(rawValue: 0){
//            print(notificationSettings.types)
//            
//            GlobalVariables.notificationvalue = 1
//            
//            let deviceTokenValue = "7eba6f12589ea1d618359dc10d5633e031aae26a50023e531f712659975a7013"
//            
//                       UserDefaults.standard.setValue(deviceTokenValue, forKey:"device_key")
//            //  self.showalert2("Please first turn on Notification from Settings.")
//            
//            
//        }else{
//            print(notificationSettings.types)
//            
//             GlobalVariables.notificationvalue = 0
//        }
//        
//        
//    }
//    
//    
//    
//    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        // Convert token to string
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        
//         GlobalVariables.notificationvalue = 0
//        print(deviceTokenString)
//        // let device_id = UIDevice.currentDevice().identifierForVendor?.UUIDString
//        
//        //  NSUserDefaults.standardUserDefaults().setObject(device_id, forKey: "DeviceId")
//         UserDefaults.standard.setValue(deviceTokenString, forKey:"device_key")
//       
//        UserDefaults.standard.synchronize()
//        
//        
//    }
//    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("APNs registration failed: \(error)")
    }
    
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
//        // Print notification payload data
//
//        print("Push notification received: \(data)")
//
//
//        if let aps = data["aps"] as? NSDictionary {
//
//            print(aps["ride_status"] as! String)
//
//            if let alert = aps["alert"] as? NSString {
//                //Do stuff
//                part1 = alert as String
//                print("Part 1: \(part1)")
//
//
//                part2 = aps["ride_id"] as! String
//                print("Part 2: \(part2)")
//
//                part3 = aps["ride_status"] as! String
//                print("Part 3: \(part3)")
//
//            }
//        }
//
//
//
//
//        if ( application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background  || application.applicationState == UIApplicationState.active )
//        {
//             AudioServicesPlaySystemSound(1005);
//
//
//
//            if(part3 == "1"){
//
//                let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
//
//                GlobalVariables.rideid = part2
//
//                 UserDefaults.standard.setValue("1", forKey:"firebaseride_status")
//
//              //  GlobalVariables.firebasedriverride = 1
//
//                if (GlobalVariables.firebasedriverride == 1){
//
//
//                }else{
//
//
//
//                    APIManager.sharedInstance.delegate = self
//                    APIManager.sharedInstance.DriverSync(part2, DriverId: driverid)
//                }
//
//
//            }
//
//             if(part3 == "101"){
//
//                NsUserDefaultManager.SingeltonInstance.logOut()
//
//                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let next: SplashViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
//                var presentedVC = self.window?.rootViewController
//                while (presentedVC!.presentedViewController != nil)  {
//                    presentedVC = presentedVC!.presentedViewController
//                }
//                presentedVC!.present(next, animated: true, completion: nil)
//
//
//
//            }
//
//             if(part3 == "54"){
//
//                GlobalVariables.rideid = part2
//
//                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let nextController: RideLaterAcceptViewController = storyboard.instantiateViewController(withIdentifier: "RideLaterAcceptViewController") as! RideLaterAcceptViewController
//                var presentedVC = self.window?.rootViewController
//                while (presentedVC!.presentedViewController != nil)  {
//                    presentedVC = presentedVC!.presentedViewController
//                }
//                presentedVC!.present(nextController, animated: true, completion: nil)
//
//
//
//            }
//
//            if(part3 == "31"){
//
//            GlobalVariables.donerideid = part2
//
//
//            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let nextController: RideFareViewController = storyboard.instantiateViewController(withIdentifier: "RideFareViewController") as! RideFareViewController
//
//                var presentedVC = self.window?.rootViewController
//
//                while (presentedVC!.presentedViewController != nil)  {
//                    presentedVC = presentedVC!.presentedViewController
//                }
//                presentedVC!.present(nextController, animated: true, completion: nil)
//
//
//
//            }
//
//            if(part3 == "2"){
//               /* GlobalVariables.rideid = part2
//
//                let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
//
//                APIManager.sharedInstance.delegate = self
//                APIManager.sharedInstance.DriverSync(part2, DriverId: driverid)*/
//
//                 self.showalert("Ride has been cancelled by user".localized)
//
//
//                //  self.showalert("Ride has been cancelled by user")
//            }
//            if(part3 == "51"){
//
//
//                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let nextController: NotificationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
//
//
//                var presentedVC = self.window?.rootViewController
//
//                while (presentedVC!.presentedViewController != nil)  {
//                    presentedVC = presentedVC!.presentedViewController
//                }
//                presentedVC!.present(nextController, animated: true, completion: nil)
//
//
//
//
//            }
//
//
//            if(part3 == "52"){
//
//
//                self.showalert10(message: part1)
//            }
//
//
//
//
//
//            if(part3 == "20"){
//                GlobalVariables.rideid = part2
//
//                    let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
//
//                APIManager.sharedInstance.delegate = self
//                APIManager.sharedInstance.DriverSyncHomeScreeen(DriverId: driverid)
//
//               // APIManager.sharedInstance.delegate = self
//               // APIManager.sharedInstance.DriverSync(part2, DriverId: driverid)
//
//
//
//            }
//
//
//            if(part3 == "8"){
//
//                let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
//
//                GlobalVariables.rideid = part2
//
//                APIManager.sharedInstance.delegate = self
//                APIManager.sharedInstance.DriverSync(part2, DriverId: driverid)
//
//
//            }
//
//
//            if(part3 == "10"){
//
//
//              //  GlobalVariables.rideid = part2
//
//              //  GlobalVariables.firebasedriverride = 1
//
//                 UserDefaults.standard.setValue("10", forKey:"firebaseride_status")
//
//                if (GlobalVariables.firebasedriverride == 1){
//
//
//                }else{
//
//               GlobalVariables.rideid = part2
//
//                APIManager.sharedInstance.delegate = self
//                APIManager.sharedInstance.RentalDriverSync(RentalBookindId: part2)
//                }
//
//            }
//
//            if(part3 == "15"){
//
//
//                GlobalVariables.rideid = part2
//
//                  self.showalert("Ride has been cancelled by user".localized)
//
//             //   APIManager.sharedInstance.delegate = self
//                // APIManager.sharedInstance.RentalDriverSync(RentalBookindId: part2)
//
//
//            }
//
//            if(part3 == "17"){
//
//
//              //  let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
//
//             //   GlobalVariables.rideid = part2
//
//                self.showalert("Ride has been cancelled by Admin".localized)
//
//
//              //  APIManager.sharedInstance.delegate = self
//              //  APIManager.sharedInstance.DriverSync(part2, DriverId: driverid)
//
//            }
//
//
//            if(part3 == "19"){
//
//
//             //   GlobalVariables.rideid = part2
//
//                self.showalert("Ride has been cancelled by Admin".localized)
//
//
//              //  APIManager.sharedInstance.delegate = self
//              //  APIManager.sharedInstance.RentalDriverSync(RentalBookindId: part2)
//
//
//            }
//
//
//        }
//
    
  //  }
    
    
    func showalert10(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title: "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                  GlobalVariables.dialogopen = "0"
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: NewRequestViewController = storyboard.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.present(nextController, animated: true, completion: nil)
                
            }
            alertController.addAction(OKAction)
            
            var presentedVC = self.window?.rootViewController
            
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            
            presentedVC!.present(alertController, animated: true) {
                
            }
            
            
            
            
        })
        
    }
    

    

func showalert(_ message:String)  {
    
    DispatchQueue.main.async(execute: {
        
        let alertController = UIAlertController(title: "Cancelled".localized, message:message, preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "ok", style: .default) { (action) in
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextController: OnLineViewController = storyboard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
            var presentedVC = self.window?.rootViewController
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            presentedVC!.present(nextController, animated: true, completion: nil)
            
        }
        alertController.addAction(OKAction)
        var presentedVC = self.window?.rootViewController
        while (presentedVC!.presentedViewController != nil)  {
            presentedVC = presentedVC!.presentedViewController
        }
        presentedVC!.present(alertController, animated: true){
            
            
            //self.presentViewController(alertController, animated: true) {
            
        }
        
        
    })
    
    
    
}
    
    func showalert5(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:  "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
                
                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
                
            }
            alertController.addAction(OKAction)
            
            var presentedVC = self.window?.rootViewController
            
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            
            presentedVC!.present(alertController, animated: true) {
                
            }

                
        
            
        })
        
    }




func showalert1(_ message:String)  {
    
    DispatchQueue.main.async(execute: {
        
        let alertController = UIAlertController(title: "Alert".localized, message:message, preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextController: OnLineViewController = storyboard.instantiateViewController(withIdentifier: "OnLineViewController") as! OnLineViewController
            var presentedVC = self.window?.rootViewController
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            presentedVC!.present(nextController, animated: true, completion: nil)
            
            
            
        }
        alertController.addAction(OKAction)
        
        var presentedVC = self.window?.rootViewController
        
        while (presentedVC!.presentedViewController != nil)  {
            presentedVC = presentedVC!.presentedViewController
        }
        
        presentedVC!.present(alertController, animated: true) {
            
        }
        
        
    })
    
}
    
    func shownewalert1(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title: "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: TrackRideViewController = storyboard.instantiateViewController(withIdentifier: "TrackRideViewController") as! TrackRideViewController
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.present(nextController, animated: true, completion: nil)
                
                
                
                
            }
            alertController.addAction(OKAction)
            
            var presentedVC = self.window?.rootViewController
            
            while (presentedVC!.presentedViewController != nil)  {
                presentedVC = presentedVC!.presentedViewController
            }
            
            presentedVC!.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }


func onSuccessState(_ data: AnyObject , resultCode: Int) {
    
    
        
    if resultCode == 2006 {
        
        driversyncdata = data as! DriverSyncModel
        
        if(driversyncdata.result == 0){
            
            self.showalert1("Your Notification has been expired!!".localized)
            
            
        }else
        {
            if(driversyncdata.details?.rideStatus == "1"){
                
                print("Ride Id \(GlobalVariables.rideid)")
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: AcceptRejectViewController = storyboard.instantiateViewController(withIdentifier: "AcceptRejectViewController") as! AcceptRejectViewController
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.present(nextController, animated: true, completion: nil)
                
                
            }
            
           if(driversyncdata.details?.rideStatus == "8"){
                
                print("Ride Id \(GlobalVariables.rideid)")
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: AcceptRejectViewController = storyboard.instantiateViewController(withIdentifier: "AcceptRejectViewController") as! AcceptRejectViewController
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.present(nextController, animated: true, completion: nil)
                
                
            }
            
            
            if(driversyncdata.details?.rideStatus == "2"){
                
                self.showalert("Ride has been cancelled by user".localized)
            }
            
            if(driversyncdata.details?.rideStatus == "17"){
                
                self.showalert("Ride has been cancelled by Admin".localized)
            }

            
            
        
            
            
        }
        
        
    }
    
    
    if resultCode == 8006 {
        
        rentalridesyncdata = data as! RentalRideSyncModel
        
     
        
        if(rentalridesyncdata.status == 0){
            
            self.showalert1("Your Notification has been expired!!".localized)
            
            
        }else
        {
            
            if(rentalridesyncdata.details?.bookingStatus == "10"){
                
                
                print("Ride Id \(GlobalVariables.rideid)")
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextController: RentalAcceptRejectViewController = storyboard.instantiateViewController(withIdentifier: "RentalAcceptRejectViewController") as! RentalAcceptRejectViewController
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.present(nextController, animated: true, completion: nil)
                
                             
            }
            
            
            if(rentalridesyncdata.details?.bookingStatus == "15"){
                
                self.showalert("Ride has been cancelled by user".localized)
            }
            
            if(rentalridesyncdata.details?.bookingStatus == "19"){
                
                self.showalert("Ride has been cancelled by Admin".localized)
            }

            
            
            
        }
        
        
    }
    
    if (resultCode == 8019){
        
        driversyncdata = data as! DriverSyncModel
        
        if(driversyncdata.result == 0){
            
            self.showalert1("Your Notification has been expired!!".localized)
            
        }else
        {

            
              if(driversyncdata.details?.rideStatus == "3"){
                
                
                 self.shownewalert1(message: "Drop location has been changed by customer".localized)
            }
            
              if(driversyncdata.details?.rideStatus == "5"){
                
                
                 self.shownewalert1(message: "Drop location has been changed by customer".localized)
            }
            
              if(driversyncdata.details?.rideStatus == "6"){
                
                 self.shownewalert1(message: "Drop location has been changed by customer".localized)
                
            }
            
            
        }
        
        
    }

    
}

}








