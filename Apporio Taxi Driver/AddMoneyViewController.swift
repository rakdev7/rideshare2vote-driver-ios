//
//  AddMoneyViewController.swift
//  TaxiUser
//
//  Created by AppOrio on 15/07/17.
//  Copyright © 2017 apporio. All rights reserved.
//

import UIKit


class AddMoneyViewController: UIViewController,ParsingStates {
    
    @IBOutlet weak var mainview: UIView!
    
   // @IBOutlet weak var topview: UIView!
    
    
    
  //  var savedata : SaveCardModel!
  //  var carddata : CardDetailsModel!
    var addmoneydata : AddMoneyModel!
    
    var SIZE = 0
    
    var toastLabel : UILabel!

    var enteramountvalue = ""
    
   
     let driverid = NsUserDefaultManager.SingeltonInstance.getuserdetails(key: NsUserDefaultManager.KeyDriverid)!
    
    
   // @IBOutlet weak var showwalletmoneytext: UILabel!
    @IBOutlet var lblAddMoney: UILabel!
    @IBOutlet var lblAddMoneyToWallet: UILabel!
    @IBOutlet var lblItQuickAndSafe: UILabel!
    
  //  @IBOutlet var lblCardDetails: UILabel!
    
       @IBOutlet var btnAddMoney: UIButton!
    
   // @IBOutlet weak var carddetailstableview: UITableView!
    
    
    @IBOutlet weak var currencylabeltext: UILabel!
    
    
    @IBOutlet weak var firstbtn: UIButton!
    
    @IBOutlet weak var secondbtn: UIButton!
    
    @IBOutlet weak var thirdbtn: UIButton!
    @IBOutlet weak var enteramounttext: UITextField!
    
    
    
    var newGeneratedCardName = ""
    var newGeneratedCardNumber = ""
    var newGeneratedCardExpiryMonth = ""
    var newGeneratedCardExpiryYear = ""
    var newGeneratedCardCv = ""
    
    func setColor() {
        btnAddMoney.backgroundColor = AppColors.themeColor
    }
    
    func viewSetupForLang(){
        setColor()
        lblAddMoney.text = "Add Money To Wallet".localized
        lblAddMoneyToWallet.text = "Add Money To Wallet".localized
        lblItQuickAndSafe.text = "It's quick,safe and secure".localized
        btnAddMoney.setTitle("Add Money".localized, for: UIControlState.normal)
        
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSetupForLang()
        
        
        //carddetailstableview.isHidden = true
        
        
        currencylabeltext.text = GlobalVariables.currencysymbol
        
        self.firstbtn.setTitle(GlobalVariables.currencysymbol + " 100", for: .normal)
        
         self.secondbtn.setTitle(GlobalVariables.currencysymbol + " 200", for: .normal)
        
         self.thirdbtn.setTitle(GlobalVariables.currencysymbol + " 300", for: .normal)
        
       
        //  enteramounttext.becomeFirstResponder()
        
       
       
        
        mainview.layer.shadowColor = UIColor.gray.cgColor
        mainview.layer.shadowOpacity = 1
        mainview.layer.shadowOffset = CGSize(width: 0, height: 2)
        mainview.layer.shadowRadius = 2
        
        
        self.firstbtn.layer.borderWidth = 1.0
        self.firstbtn.layer.cornerRadius = 4
        
        self.secondbtn.layer.borderWidth = 1.0
        self.secondbtn.layer.cornerRadius = 4
        self.thirdbtn.layer.borderWidth = 1.0
        self.thirdbtn.layer.cornerRadius = 4
       

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if GlobalVariables.Walletcheck == 3{

            GlobalVariables.Walletcheck = 0
            
            APIManager.sharedInstance.delegate = self
            APIManager.sharedInstance.AddMoneyMethod(DriverId: driverid, Amount: enteramountvalue, CardId: GlobalVariables.CardId)

         


        }else{


        }
        
    }
    
    @IBAction func backbtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    @IBAction func firstbtn_click(_ sender: Any) {
        
        firstbtn.backgroundColor = UIColor(red:147.0/255.0, green:165.0/255.0, blue:165.0/255.0, alpha:1.0)
        
      secondbtn.backgroundColor = UIColor.clear
        thirdbtn.backgroundColor = UIColor.clear
        
       
        enteramounttext.text = "100"
    }
    
    @IBAction func secondbtn_click(_ sender: Any) {
        
        
        secondbtn.backgroundColor = UIColor(red:147.0/255.0, green:165.0/255.0, blue:165.0/255.0, alpha:1.0)
        firstbtn.backgroundColor = UIColor.clear
        thirdbtn.backgroundColor = UIColor.clear
     
        

        
         enteramounttext.text = "200"
    }
    
    @IBAction func thirdbtn_click(_ sender: Any) {
        
        
        thirdbtn.backgroundColor = UIColor(red:147.0/255.0, green:165.0/255.0, blue:165.0/255.0, alpha:1.0)
        
        secondbtn.backgroundColor = UIColor.clear
        firstbtn.backgroundColor = UIColor.clear
        

                 enteramounttext.text = "300"
        
        
    }
    
    
  
    @IBAction func AddMoney_btn(_ sender: Any) {
        
        enteramountvalue = enteramounttext.text!
        
        if enteramountvalue == "" {
            
            self.showalert(message: "Please enter Amount First".localized)
            
        }else{
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let walletpaymentoptionviewcontroller = storyBoard.instantiateViewController(withIdentifier: "ACViewController") as! ACViewController
            // walletpaymentoptionviewcontroller.viewcontrollerself = self
            // walletpaymentoptionviewcontroller.modalPresentationStyle = .overCurrentContext
            
            self.present( walletpaymentoptionviewcontroller, animated:true, completion:nil)
            
        }

        
    }
    
    
    func showalert(message:String)  {
        
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title:   "Alert".localized, message:message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action) in
                
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true) {
                
            }
            
            
        })
        
    }
    
    
   
    
    

    func onSuccessState(_ data: AnyObject , resultCode: Int) {
        
        if resultCode == 8042{
            
            if let addmoneydata = data as? AddMoneyModel{
                
                self.addmoneydata = addmoneydata
                
                if(addmoneydata.result == 1){
                    GlobalVariables.addmoneyvalue = 1
                     self.dismiss(animated: true, completion: nil)
                    
                }else{
                    
                    self.showalert(message: addmoneydata.msg!)
                    
                    
                }
                
                
            }
            
        }
        
    }
    
}
